#include <psp2kern/kernel/cpu.h>
#include <psp2kern/kernel/modulemgr.h>
#include <psp2kern/kernel/threadmgr.h>
#include <psp2kern/kernel/sysmem.h>
#include <psp2kern/io/fcntl.h>
#include <psp2kern/display.h>
#include "log.h"

void _start() __attribute__ ((weak, alias ("module_start")));

#define ALIGN(x, a) (((x) + ((a) - 1)) & ~((a) - 1))

#define SCRATCHPAD_ADDR ((void *)0x00000000)
#define EXCEPTION_VECTORS_SIZE 0x20
#define SCREEN_PITCH 1024
#define SCREEN_W 960
#define SCREEN_H 544

extern int ksceKernelGetPaddr(unsigned long vaddr, unsigned long *paddr);

extern const unsigned char _binary_payload_bin_start;
extern const unsigned char _binary_payload_bin_size;

static const unsigned int payload_size = (unsigned int)&_binary_payload_bin_size;
static const void *const payload_addr = (void *)&_binary_payload_bin_start;

static unsigned long get_cpu_id(void);
static void set_vbar(unsigned long vbar);
static unsigned long get_ttbr0(void);
static unsigned long get_ttbcr(void);
static unsigned long get_paddr(unsigned long vaddr);
static int find_paddr(unsigned long paddr, unsigned long vaddr, unsigned int size,
		      unsigned int step, unsigned long *found_vaddr);
static unsigned long page_table_entry(unsigned long paddr);
static void map_scratchpad(void);
static int map_framebuffer(void);
static void unmap_framebuffer(void);

static SceUID framebuffer_uid = -1;

static int payload_trampoline_thread(SceSize args, void *argp)
{
	int flags;

	//ksceKernelDelayThread(1000);

	/*asm volatile("cpsid if\n");

	#define FB_ADDR     0x21200000
	#define SCREEN_STRIDE   1024
	#define SCREEN_W    960
	#define SCREEN_H    544

	int cpu_id = get_cpu_id();
	unsigned int cnt[4] = {0, 0, 0, 0};

	while (1) {
		int i, j;
		for (i = (SCREEN_H / 4) * cpu_id; i < (SCREEN_H / 4) * cpu_id + (SCREEN_H / 4); i++) {
			for (j = 0; j < SCREEN_W; j++) {
				((volatile unsigned int *)FB_ADDR)[j + i * SCREEN_STRIDE] =
					((cnt[cpu_id] & 0xFF) << ((cpu_id % 3) * 8)) | 0xFF000000;
			}
		}
		cnt[cpu_id] += (cpu_id + 1) * 2;
	}*/

	/*
	 * Set up the exception vectors to make them jump to our payload.
	 * SCTLR.V is already 0 (Low exception vectors, base address 0x00000000)
	 * SCTLR.VE is already 0 (Use the FIQ and IRQ vectors from the vector table)
	 * The Vita has Security Extensions, so VBAR is used.
	 */
	flags = ksceKernelCpuDisableInterrupts();

	asm volatile(
		"mov r0, #0\n"
		/* TLB invalidate */
		"mcr p15, 0, r0, c8, c6, 0\n"
		"mcr p15, 0, r0, c8, c5, 0\n"
		"mcr p15, 0, r0, c8, c7, 0\n"
		"mcr p15, 0, r0, c8, c3, 0\n"
		/* Branch predictor invalidate all */
		"mcr p15, 0, r0, c7, c5, 6\n"
		/* Branch predictor invalidate all (IS) */
		"mcr p15, 0, r0, c7, c1, 6\n"
		/* Instruction cache invalidate all (PoU) */
		"mcr p15, 0, r0, c7, c5, 0\n"
		/* Instruction cache invalidate all (PoU, IS) */
		"mcr p15, 0, r0, c7, c1, 0\n"
		/* Instruction barrier */
		"mcr p15, 0, r0, c7, c5, 4\n"
		/* Data sync barrier */
		"dsb\n"
		: : : "r0");


	set_vbar((unsigned long)SCRATCHPAD_ADDR);

	/*
	 * Trigger SVC to jump to our payload
	 */
	asm volatile("svc #0\n");

	while (1)
		;

	ksceKernelCpuEnableInterrupts(flags);

	return 0;
}

int module_start(SceSize argc, const void *args)
{
	int ret;

	log_reset();
	LOG("IRQ test by xerpi\n");
	LOG("Payload size: 0x%08X\n", payload_size);

	/*
	 * Map the scratchpad to VA 0x00000000-0x00007FFF.
	 */
	map_scratchpad();
	LOG("Scratchpad mapped\n");

	/*
	 * Copy the payload (including exception vectors) to the scratchpad.
	 */
	ksceKernelCpuUnrestrictedMemcpy(SCRATCHPAD_ADDR, payload_addr, payload_size);
	ksceKernelCpuDcacheWritebackRange(SCRATCHPAD_ADDR, payload_size);
	ksceKernelCpuIcacheAndL2WritebackInvalidateRange(SCRATCHPAD_ADDR, payload_size);

	/*
	 * Map the framebuffer.
	 */
	ret = map_framebuffer();
	if (ret < 0) {
		LOG("Error mapping the framebuffer: 0x%08X\n", ret);
		return SCE_KERNEL_START_FAILED;
	}
	LOG("Framebuffer mapped\n");
	LOG("\n");

	/*
	 * Start a thread for each CPU.
	 */
	int i;
	for (i = 0; i < 4; i++) {
		SceUID thid = ksceKernelCreateThread("trampoline",
			payload_trampoline_thread, 0x00, 0x1000, 0, 1 << i, 0);

		ksceKernelStartThread(thid, 0, NULL);
	}

	/*
	 * Wait forever...
	 */
	while (1)
		ksceKernelDelayThread(1000);

	unmap_framebuffer();

	return SCE_KERNEL_START_SUCCESS;

}

int module_stop(SceSize argc, const void *args)
{
	return SCE_KERNEL_STOP_SUCCESS;
}

unsigned long get_cpu_id(void)
{
	unsigned long mpidr;

	asm volatile("mrc p15, 0, %0, c0, c0, 5\n" : "=r"(mpidr));

	return mpidr & 3;
}

void set_vbar(unsigned long vbar)
{
	asm volatile("mcr p15, 0, %0, c12, c0, 0\n" : : "r"(vbar));
}

unsigned long get_vector_base_address(void)
{
	unsigned long address;

	asm volatile("mrc p15, 0, %0, c12, c0, 0\n" : "=r"(address));

	return address;
}

unsigned long get_ttbr0(void)
{
	unsigned long ttbr0;

	asm volatile("mrc p15, 0, %0, c2, c0, 0\n" : "=r"(ttbr0));

	return ttbr0;
}

unsigned long get_ttbcr(void)
{
	unsigned long ttbcr;

	asm volatile("mrc p15, 0, %0, c2, c0, 2\n" : "=r"(ttbcr));

	return ttbcr;
}
unsigned long get_paddr(unsigned long vaddr)
{
	unsigned long paddr;

	ksceKernelGetPaddr(vaddr, &paddr);

	return paddr;
}

int find_paddr(unsigned long paddr, unsigned long vaddr, unsigned int size, unsigned int step, unsigned long *found_vaddr)
{
	unsigned long vaddr_end = vaddr + size;

	for (; vaddr < vaddr_end; vaddr += step) {
		unsigned long cur_paddr = get_paddr(vaddr);

		if ((cur_paddr & ~(step - 1)) == (paddr & ~(step - 1))) {
			if (found_vaddr)
				*found_vaddr = vaddr;
			return 1;
		}
	}

	return 0;
}

unsigned long page_table_entry(unsigned long paddr)
{
	unsigned long base_addr = paddr >> 12;
	unsigned long XN        = 0b0;   /* XN disabled */
	unsigned long C_B       = 0b11;  /* Inner cacheable memory: Write-Back, no Write-Allocate */
	unsigned long AP_1_0    = 0b11;  /* Full access */
	unsigned long TEX_2_0   = 0b111; /* Outer cacheable memory: Write-Back, no Write-Allocate */
	unsigned long AP_2      = 0b0;   /* Full access */
	unsigned long S         = 0b1;   /* Shareable */
	unsigned long nG        = 0b0;   /* Global translation */

	return  (base_addr << 12) |
		(nG        << 11) |
		(S         << 10) |
		(AP_2      <<  9) |
		(TEX_2_0   <<  6) |
		(AP_1_0    <<  4) |
		(C_B       <<  2) |
		(1         <<  1) |
		(XN        <<  0);
}

void map_scratchpad(void)
{
	int i;
	unsigned long ttbcr_n;
	unsigned long ttbr0_paddr;
	unsigned long ttbr0_vaddr;
	unsigned long first_page_table_paddr;
	unsigned long first_page_table_vaddr;
	unsigned long pt_entries[4];

	/*
	 * Identity-map the scratchpad (PA 0x00000000-0x00007FFF) to
	 * the VA 0x00000000-0x00007FFF.
	 * To do such thing we will use the first 4 PTEs of the
	 * first page table of TTBR0 (which aren't used).
	 */

	ttbcr_n = get_ttbcr() & 7;
	ttbr0_paddr = get_ttbr0() & ~((1 << (14 - ttbcr_n)) - 1);
	find_paddr(ttbr0_paddr, 0, 0xFFFFFFFF, 0x1000, &ttbr0_vaddr);

	first_page_table_paddr = (*(unsigned int *)ttbr0_vaddr) & 0xFFFFFC00;
	find_paddr(first_page_table_paddr, 0, 0xFFFFFFFF, 0x1000, &first_page_table_vaddr);

	/*LOG("ttbr0_paddr: 0x%08lX\n", ttbr0_paddr);
	LOG("ttbr0_vaddr: 0x%08lX\n", ttbr0_vaddr);
	LOG("First page table paddr: 0x%08lX\n", first_page_table_paddr);
	LOG("First page table vaddr: 0x%08lX\n", first_page_table_vaddr);
	LOG("\n");*/

	for (i = 0; i < 4; i++) {
		pt_entries[i] = page_table_entry(i << 12);
	}

	ksceKernelCpuUnrestrictedMemcpy((void *)first_page_table_vaddr, pt_entries, sizeof(pt_entries));
	ksceKernelCpuDcacheAndL2WritebackRange((void *)first_page_table_vaddr, sizeof(pt_entries));
}

int map_framebuffer(void)
{
	const unsigned int fb_size = ALIGN(4 * SCREEN_PITCH * SCREEN_H, 256 * 1024);
	int ret;
	SceDisplayFrameBuf fb;
	void *fb_addr;

	framebuffer_uid = ksceKernelAllocMemBlock("fb", 0x40404006 , fb_size, NULL);
	if (framebuffer_uid < 0)
		return framebuffer_uid;

	ret = ksceKernelGetMemBlockBase(framebuffer_uid, &fb_addr);
	if (ret < 0)
		return ret;

	memset(fb_addr, 0xFF, 4 * SCREEN_PITCH * SCREEN_H);
	ksceKernelCpuDcacheAndL2WritebackRange(fb_addr, 4 * SCREEN_PITCH * SCREEN_H);

	LOG("Framebuffer uid: 0x%08X\n", framebuffer_uid);
	LOG("Framebuffer vaddr: 0x%08X\n", (uintptr_t)fb_addr);
	LOG("Framebuffer paddr: 0x%08lX\n", get_paddr((uintptr_t)fb_addr));
	LOG("Framebuffer size: 0x%08X\n", fb_size);

	memset(&fb, 0, sizeof(fb));
	fb.size        = sizeof(fb);
	fb.base        = fb_addr;
	fb.pitch       = SCREEN_PITCH;
	fb.pixelformat = SCE_DISPLAY_PIXELFORMAT_A8B8G8R8;
	fb.width       = SCREEN_W;
	fb.height      = SCREEN_H;

	return ksceDisplaySetFrameBuf(&fb, SCE_DISPLAY_SETBUF_NEXTFRAME);
}

void unmap_framebuffer(void)
{
	if (framebuffer_uid >= 0)
		ksceKernelFreeMemBlock(framebuffer_uid);
}
