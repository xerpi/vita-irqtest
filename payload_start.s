.set ARM_MODE_IRQ, 0x12
.set ARM_MODE_SYS, 0x1F
.set FIQ_DISABLE,  0x40
.set IRQ_DISABLE,  0x80

	.cpu cortex-a9
	.align 4
	.code 32

	.section .text.start

	.global _start
_start:

# This is loaded at address 0, and the exception vectors are configured to be there
_exception_vectors_table:
	ldr pc, _reset_h
	ldr pc, _undefined_instruction_vector_h
	ldr pc, _software_interrupt_vector_h
	ldr pc, _prefetch_abort_vector_h
	ldr pc, _data_abort_vector_h
	ldr pc, _reserved_handler_h
	ldr pc, _irq_vector_h
	ldr pc, _fiq_vector_h

_reset_h:                        .word payload_start
_undefined_instruction_vector_h: .word _stub_interrupt_vector_handler
_software_interrupt_vector_h:    .word payload_start
_prefetch_abort_vector_h:        .word _stub_interrupt_vector_handler
_data_abort_vector_h:            .word _stub_interrupt_vector_handler
_reserved_handler_h:             .word _stub_interrupt_vector_handler
_irq_vector_h:                   .word _irq_handler
_fiq_vector_h:                   .word _fiq_handler

_stub_interrupt_vector_handler:
	subs pc, lr, #4

_irq_handler:
	sub lr, lr, #4
	stmfd sp!, {r0-r12, lr}
	bl irq_routine
	ldmfd sp!, {r0-r12, pc}^

_fiq_handler:
	sub lr, lr, #4
	stmfd sp!, {r0-r12, lr}
	bl fiq_routine
	ldmfd sp!, {r0-r12, pc}^

payload_start:
	# Disable interrupts
	cpsid if

	# DACR unrestricted
	ldr r0, =0xFFFFFFFF
	mcr p15, 0, r0, c3, c0, 0

	# Get CPU ID
	mrc p15, 0, r0, c0, c0, 5
	and r0, r0, #3

	# Set IRQ stack (0x400 of stack for each CPU)
	msr cpsr_c, #(ARM_MODE_IRQ | IRQ_DISABLE | FIQ_DISABLE)
	ldr sp, =_irq_stack_start
	add sp, #0x400
	add sp, r0, lsl #10

	# Set SYS mode stack (0x400 of stack for each CPU)
	msr cpsr_c, #(ARM_MODE_SYS | IRQ_DISABLE | FIQ_DISABLE)
	ldr sp, =_sys_stack_start
	add sp, #0x400
	add sp, r0, lsl #10

	# Read CPU ID
	mrc p15, 0, r0, c0, c0, 5
	and r0, r0, #3

	# Notify that we are here
	mov r1, #1
	ldr r2, =cpu_sync
	str r1, [r2, r0, lsl #2]

	# If we are the CPU[1, 3], then WFI
	cmp r0, #0
	beq cpu0_continue

	# Wait until CPU0 tells us to continue
	ldr r0, =cpus_cont
cpu1_3_wait:
	ldr r1, [r0]
	cmp r1, #0
	beq cpu1_3_wait

	b jump_to_main

cpu0_continue:
	# Wait until all the CPUs have reached the payload_code
	ldr r0, =cpu_sync
1:
	ldr r1, [r0, #0x0]
	ldr r2, [r0, #0x4]
	ldr r3, [r0, #0x8]
	ldr r4, [r0, #0xC]
	add r1, r1, r2
	add r1, r1, r3
	add r1, r1, r4
	cmp r1, #4
	blt 1b

	# Disable private timer and watchdog
	ldr r0, =0x1A002000
	mov r1, #0
	str r1, [r0, #(0x0600 + 0x08)]
	str r1, [r0, #(0x0600 + 0x28)]

	# Unlock all L2 cache lines
	ldr r0, =0x1A002000
	ldr r1, =0xFFFF
	str r1, [r0, #0x954]
	dmb

	# Clean and invalidate L2 cache
	str r1, [r0, #0x7FC]
	dmb

	# L2 cache sync
	mov r1, #0
	str r1, [r0, #0x730]
1:
	ldr r1, [r0, #0x730]
	tst r1, #1
	bne 1b
	dmb

	# Disable L2 cache
	mov r1, #0
	str r1, [r0, #0x100]
	dmb

	# Tell CPU1-3 to continue
	ldr r0, =cpus_cont
	mov r1, #1
	str r1, [r0]

jump_to_main:

	# Now let's to disable the MMU and the data cache.
	# Since we are in an identity-mapped region, it should be ok.
	mrc p15, 0, r0, c1, c0, 0
	bic r0, #0b101
	mcr p15, 0, r0, c1, c0, 0

	# Since we are at the page 0, we can easily disable TTBR1
	# by setting TTBCR.N to 0.
	mrc p15, 0, r0, c2, c0, 2
	bic r0, #1 << 31
	bic r0, #(7 << 0) | (1 << 4)
	mcr p15, 0, r0, c2, c0, 2

	# Jump to main
	ldr lr, =main
	bx lr

# Variables
cpu_sync:
	.word 0
	.word 0
	.word 0
	.word 0

cpus_cont:
	.word 0

	.ltorg
